// Plugins
const markdown = require("markdown-it");
const anchor = require("markdown-it-anchor");
const htmlMinify = require("html-minifier-terser");

// Functions
const dateConversion = require("./date.js");
const stringFormatter = require("./formatter.js");
const generalTools = require("./tools.js");

module.exports = function (eleventyConfig) {
    eleventyConfig.addPassthroughCopy("src/**/*.jpg");
    eleventyConfig.addPassthroughCopy("src/**/*.jpeg");
    eleventyConfig.addPassthroughCopy("src/**/*.png");
    eleventyConfig.addPassthroughCopy("src/**/*.webp");
    eleventyConfig.addPassthroughCopy("src/**/*.asc");

    eleventyConfig.addFilter("stringDate", function (value) {
        return dateConversion.string(value);
    });
    eleventyConfig.addFilter("rfcDate", function (value) {
        return dateConversion.rfc822(value);
    });
    eleventyConfig.addFilter("isoDate", function (value) {
        return dateConversion.iso8601(value);
    });
    eleventyConfig.addFilter("age", function (value) {
        return dateConversion.age(value);
    });
    eleventyConfig.addFilter("favicon", async function (value) {
        return await generalTools.favicon(value);
    });
    eleventyConfig.addFilter("thumbnail", async function (value) {
        return await stringFormatter.thumbnail(value);
    });

    eleventyConfig.addShortcode(
        "social",
        function (id, name = "", classes = "", extra = "", tracking = "") {
            return stringFormatter.social(id, name, classes, extra, tracking);
        },
    );
    eleventyConfig.addShortcode(
        "image",
        async function (
            file,
            description,
            caption,
            orientation = "horizontal",
        ) {
            return await stringFormatter.image(
                file,
                description,
                caption,
                orientation,
            );
        },
    );
    eleventyConfig.addShortcode("post", function (post) {
        return stringFormatter.post(post);
    });
    eleventyConfig.addShortcode("footnote", function (id, pageURL) {
        return stringFormatter.footnote(id, pageURL);
    });

    // Custom Markdown library, for e. g. headings with ids
    let markdownLibrary = markdown({
        html: true,
        breaks: false,
        linkify: false,
    }).use(anchor, {
        // IDs
        permalink: anchor.permalink.headerLink({ class: "silent" }),
    });
    eleventyConfig.setLibrary("md", markdownLibrary);

    // Remove whitespaces, unwanted links in selected files
    eleventyConfig.addTemplateFormats("css");
    eleventyConfig.addExtension("css", {
        outputFileExtension: "css",
        useLayouts: false,

        compile: async (inputContent) => {
            return async () => {
                return inputContent
                    .replace(/\/\*.*?\*\//g, "") // Comments
                    .replace(/:\s/g, ":") // Whitespace at :
                    .replace(/,\s/g, ",") // Whitespace at ,
                    .replace(/\s{\s/g, "{") // Whitespace at {
                    .replace(/\s}\s/g, "}") // Whitespace at }
                    .replace(/  +/g, "") // Indentation
                    .replace(/^\s*$(?:\r\n?|\n)/gm, "") // Empty lines
                    .replace(/\n/g, ""); // Line breaks
            };
        },
    });
    eleventyConfig.addTemplateFormats("svg");
    eleventyConfig.addExtension("svg", {
        outputFileExtension: "svg",
        useLayouts: false,

        compile: async (inputContent) => {
            return async () => {
                return inputContent
                    .replace(/<!--.*?-->/g, "") // Comments
                    .replace(/  +/g, "") // Whitespaces
                    .replace(/^\s*$(?:\r\n?|\n)/gm, "") // Empty lines
                    .replace(/\n/g, ""); // Line breaks
            };
        },
    });
    eleventyConfig.addTransform("html", function (content) {
        if ((this.page.outputPath || "").endsWith(".html")) {
            let modified = htmlMinify.minify(content, {
                useShortDoctype: false,
                removeComments: true,
                collapseWhitespace: true,
            });

            return modified;
        }

        // Return non-HTML without modifications
        return content;
    });
    eleventyConfig.addTransform("rss", function (content) {
        if ((this.page.outputPath || "").endsWith("feed.xml")) {
            let modified = content
                .replace(/&lt;a class=&quot;silent&quot; (.*?)&gt;/g, "") // Beginning of link in heading
                .replace(/&lt;\/a&gt;&lt;\/h/g, "&lt;/h"); // End of link in heading

            return modified;
        }

        // Return non-feeds without modifications
        return content;
    });

    // Custom Languages
    eleventyConfig.addTemplateFormats("txt");
    eleventyConfig.addExtension("txt", {
        compile: async (inputContent) => {
            // Don't process file content, only frontmatter
            return async () => {
                return inputContent;
            };
        },
    });

    eleventyConfig.setNunjucksEnvironmentOptions({
        throwOnUndefined: false,
        trimBlocks: true,
        lstripBlocks: true,
    });

    return {
        markdownTemplateEngine: "njk",
        dir: {
            input: "src",
            output: "build",
        },
    };
};
