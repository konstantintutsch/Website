---
layout: app
title: Caffeine
description: Caffeine is a utility for coffee enthusiasts. It's sole purpose is to calculate the extraction ratio of a coffee.
icon: com.konstantintutsch.Caffeine.svg
download: https://flathub.org/apps/com.konstantintutsch.Caffeine
images:
    - Caffeine/com.konstantintutsch.Caffeine.Screenshot.Light.png
    - Caffeine/com.konstantintutsch.Caffeine.Screenshot.Dark.png
orientation: vertical
---

Calculate your coffee

_Caffeine_ is a utility for coffee enthusiasts. It's sole purpose is to calculate the extraction ratio of a coffee.

For example, an espresso brewed from **10g** of coffee ground weighing **18g** would have an extraction ratio of **1 : 1.8**.
