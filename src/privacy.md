---
layout: page
title: Privacy
description: The privacy policy for my website.
---

## Storage

HTTP request data is stored in Tuusula, Finland in a data center operated by [Hetzner](https://www.hetzner.com/cloud/). This data is backed up on personal hardware in Germany.

All other collected data is stored on personal hardware in Germany.

## Data

Personally, I try to maintain my own privacy where possible. Therefore, respecting the privacy of visitors to my website is a must.

Collected data does **not** contain any PII _(personally identifiable information)_ and is gathered with a custom [Umami](https://umami.is/) Analytics instance.
