---
layout: page
title: Disclaimer
description: The disclaimer for all content on my website.
---

## Opinions

All expressed [opinions]({{ site.locations.tags }}opinion/) on this site are my own. Aditionally, **my views are not permanent**. To improve, accepting new ideas and opinions is important.

Posts express my views from the time of writing them. These opinions _may_ have changed since then. A post only is a snapshot of various thoughts of mine at the time I wrote it.

## Technical details

I sometimes publish technical posts. They describe a process which _worked for me_ at the time of writing. They are not _the correct_ way to archieve a certain goal. Before following along any of my technical posts, ensure that you have a basic understanding of the post's topic.

I can not be held liable for any damage to systems, data or else.
