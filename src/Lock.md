---
layout: app
title: Lock
description: Lock is a graphical front-end for GnuPG (GPG) making use of a beautiful LibAdwaita GUI.
icon: com.konstantintutsch.Lock.svg
download: https://flathub.org/apps/com.konstantintutsch.Lock
weblate: Lock
images:
    - Lock/com.konstantintutsch.Lock.Screenshot.Light.png
    - Lock/com.konstantintutsch.Lock.Screenshot.Dark.png
---

Process data with GnuPG

_Lock_ is a graphical front-end for GnuPG (GPG) making use of a beautiful LibAdwaita GUI.

Process text and files:

- Encryption
- Decryption
- Signing
- Verification

Manage your GnuPG keyring:

- Generate new key pairs
- Import keys
- Export public keys
- View expire dates
- Update expire dates of expired keys
- Remove keys
