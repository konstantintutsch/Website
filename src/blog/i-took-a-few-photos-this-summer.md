---
layout: post
title: Wrapping-Up This Year's Summer Photography
description: This post deals with my first little photography project. Feel free to give feedback!
tags: ["post", "photography"]
date: 2024-10-20T23:13:56+02:00
index: true
images:
    - i-took-a-few-photos-this-summer/export-20240512_0008.jpg
    - i-took-a-few-photos-this-summer/export-20240512_0009.jpg
    - i-took-a-few-photos-this-summer/export-20240513_0002.jpg
    - i-took-a-few-photos-this-summer/export-20240513_0004.jpg
    - i-took-a-few-photos-this-summer/export-20240808_0001.jpg
    - i-took-a-few-photos-this-summer/export-20240810_0001.jpg
    - i-took-a-few-photos-this-summer/export-20240810_0002.jpg
footnotes:
    - The pictures are presented chronologically by the way :)
---

Okay, so, I got started with photography earlier this year. And this is the first photography project I gotten myself into. It is not _the_ biggest project with _the_ most pictures you've every seen, I can tell you that much. But isn't it quality over quantity? ;)

To be honest, I'm quite satisfied with my creations. Still, feel free to give [feedback]({{ "home.md" | inputPathToUrl }}#contact).

However, there is one thing I have to admit. This project got a _fancy_ name: “Everyday Marvels”. How _poetic_ …

Now enjoy the photos!{% footnote 1, page.url %}

{% image images[0], "Black silhouettes of gass blades in front of an orange sunset.", "Silent World", "horizontal" %}

{% image images[1], "Lit up streetlight in front of the glowing moon during twilight.", "Streetlight Symphony", "vertical" %}

{% image images[2], "A traffic mirror in front an overgrown house.", "The Mirror", "vertical" %}

{% image images[3], "A radio/television tower on a hill on the horizon. A meadow and blurred flowers are in the foreground.", "Distant", "horizontal" %}

{% image images[4], "An old streetlight illuminating leaves. Surroundings are pitch dark.", "Warmth", "vertical" %}

{% image images[5], "A mix of yellow and purple flowers between overgrown tombstones.", "Contrast", "vertical" %}

{% image images[6], "Tombstones between dried up grass blades and purple flowers.", "Abandoned Death", "horizontal" %}

_I've got no f- idea why I decided to give all these photos such titles. I can't take this seriously. Help me, please._
