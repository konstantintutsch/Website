---
layout: post
title: "Announcing Lock: a Cryptography Front-End"
description: Lock is a graphical front-end for GnuPG (GPG) making use of a beautiful LibAdwaita GUI.
tags: ["post", "privacy", "security", "programming", "open-source", "linux"]
date: 2024-11-20T12:34:42+01:00
index: true
---

Welcome! You might have already guessed this by the post's title, but still: [Lock](/Lock) is now available!

## What is Lock?

Lock is a graphical LibAdwaita front-end for GnuPG (GPG) written in C. I have been working on it for the past two months and think you should give it a try so you can discover some bugs in it.

These are the two main functionalities of Lock:

- asymmetrically **en**crypt, **decrypt**, **sign** and **verify** text or files
- manage your **GnuPG keyring** which includes **im**porting, **exporting**, **generating** and **deleting** keys

For screenshots and a more detailed description, have a look at its [README](https://github.com/konstantintutsch/Lock/blob/main/README.md) or its [Flathub](https://flathub.org/apps/com.konstantintutsch.Lock) page.

## Where can I get Lock?

On [Flathub](https://flathub.org/apps/com.konstantintutsch.Lock) using Flatpak!

You can also help [translate](https://hosted.weblate.org/engage/Lock/) and [develop](https://github.com/konstantintutsch/Lock) Lock.
