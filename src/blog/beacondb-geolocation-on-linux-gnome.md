---
layout: post
title: How-To Use BeaconDB as Your Geolocation Provider on Linux
description: Now that the Mozilla Location Service is shut down, location services are broken on most Linux systems. This post will explain how to use BeaconDB instead. Tested with GNOME, I can't gurantee that this will work in other DEs.
tags: ["post", "open-source", "linux"]
date: 2024-08-13T17:35:40+01:00
index: true
footnotes:
    - Mozilla has decided to inevitably shut down their geolocation service due to declining accuracy. https://discourse.mozilla.org/t/retiring-the-mozilla-location-service/128693
    - BeaconDB uses Ichnaea's request format. https://beacondb.net/#developers
    - This issue is well known to occur. https://beacondb.net/#usage
---

## MLS and It's Alternative

On March 13, 2024, Mozilla decided to retire their geolocation service and database.{% footnote 1, page.url %}

This is probably the reason you are currently reading this post and I can also safely assume that your system is not able to locate itself anymore. No worries though, there's a quick fix.

It's [BeaconDB](https://beacondb.net/)! An open-source alternative to the Mozilla Location Service. It's still in an experimental stage, but already pretty mature. And to our luck, also uses the same request format as Mozilla.{% footnote 2, page.url %}

## The Fix, at least with GNOME via Geoclue

On most Linux distributions, location services are provided by a service called Geoclue. Though by default, current and older versions of Geoclue are configured to use MLS as their geolocation database.

That's a problem when MLS doesn't work. And that is the case forever now. Fortunately, switching over to BeaconDB is easy.

First, find `geoclue.conf`. It's location differs on different distributions with different package manages. On Fedora, you can find it at `/etc/geoclue/geoclue.conf`.

Once found, edit the file as `root`.

```conf
[wifi]
enabled=true
url=https://beacondb.net/v1/geolocate
```

To apply the configuration change, restart the Geoclue service. On systems using SystemD, do this (and be `root`):

```
systemctl restart geoclue.service
```

That's it. Open an app that's using location services and try out your system's regained capabilities!

## Troubleshooting

If your system indefinitely tries to locate itself without any success or even fails, your area might not be indexed.{% footnote 3, page.url %}

Have a look at the [BeaconDB website](https://beacondb.net/#contribute) to find out which contribution options might be viable to you. Have fun!
