---
layout: post
title: Focal Point - London
description: London In Focus. A collection of photos from my trip to London.
tags: ["post", "photography"]
date: 2024-08-05T23:25:12+01:00
index: true
images:
    - focal-point-london/export-20240802_0016.jpg
    - focal-point-london/export-20240802_0025.jpg
    - focal-point-london/export-20240802_0024.jpg
    - focal-point-london/export-20240802_0022.jpg
    - focal-point-london/export-20240804_0003.jpg
---

_Oi, mate! <sup>(Sorry, but I had to)</sup>_

As you can probably already tell by the title, I was in London for about six days.

It's a fine city, but it didn't really blow me away. While I was there, I went to all the popular sights, had a look at some neighborhoods and tried to get an understanding of the city in general.

Towards the end of my stay I also attended ABBA Voyage. That really was an amazing experience despite the group members not actually being there.

And that was it, enjoy the photos!

{% image images[0], "A picture of two skyscrapers. The first is in the foreground and the second is in focus. The background is the blue sky with a white cloud. The cloud reflects on the first skyscraper. The second skyscraper is illuminated by the sun from one side.", "Geometry & Reflections" %}

{% image images[1], "A panorama picture from The Shard towards the northwest. Most prominent building is St Paul's Cathedral.", "Northwest – The Shard" %}

{% image images[2], "A panorama picture from The Shard towards the north. Most prominent buildings are the skyscrapers near Leadenhall St.", "North – The Shard" %}

{% image images[3], "A picture of the dome of St Paul's Cathedral from a low angle with the sun/a sunray hitting the sensor directly and producing a heavy sunflare.", "St Paul's Cathedral – with kind help from the sun", "vertical" %}

{% image images[4], "A picure of the Old Royal Naval College with the skyline of the Docklands in the background.", "Old Royal Naval College, feat. The Docklands" %}
