---
layout: post
title: Focal Point - Bath, UK
description: A collection of travel photography from Bath.
tags: ["post", "photography"]
date: 2024-08-13T11:17:33+01:00
index: true
images:
    - focal-point-bath/export-20240812_0002.jpg
    - focal-point-bath/export-20240807_0002.jpg
    - focal-point-bath/export-20240807_0004.jpg
    - focal-point-bath/export-20240808_0004.jpg
    - focal-point-bath/export-20240808_0009.jpg
---

This is the [second](/blog/focal-point-london/) and last post about the photos from my trip to the UK. Have fun scrolling through!

_Keep in mind that this is not intended to be artistic photography, these are just a few snapshots I felt like sharing._

{% image images[0], "A panorama of the downtown Bath", "Panorama of downtown Bath" %}

{% image images[1], "Crescent shaped row housing and cloudy sky", "The Royal Crescent" %}

{% image images[2], "A house with a black metal fence and white roses", "Flowers" %}

{% image images[3], "A picture of the main basin in the roman baths", "The Roman Baths", "vertical" %}

{% image images[4], "An empty street in the night illuminated by warm light from street lanterns", "The Street", "vertical" %}