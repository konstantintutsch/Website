---
layout: post
title: This blog is now powered by 11ty
description: I decided to migrate this blog to 11ty!
tags: ["post", "blog"]
date: 2024-05-16T22:11:26+02:00
index: true
---
Just a quick update on the software stack of this blog. I migrated to [11ty](https://11ty.dev) and changed all Liquid to Nunjucks.

Really excited to have made the step and also to try out something new!
