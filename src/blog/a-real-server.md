---
layout: post
title: I Got a Real Server Now
description: I own a real (you know what I mean) server now. It has replaced my Raspberry Pi and changed my attitude towards self-hosting services I would not have self-hosted before.
tags: ["post", "linux", "self-hosting"]
date: 2024-11-07T17:54:54+01:00
index: true
images:
    - a-real-server/export-20241107_0001.jpg
---

{% image images[0], "A standard server case with status LEDs blinking. Photographed in the dark.", "The <i>REAL</i> Server", "horizontal" %}

This is the new (old, actually) server that has now replaced my little Raspberry Pi. To give you an idea of it's capabilities, here is a _really_ boring list of hardware I deem important:

- an AMD64 CPU with 16 cores @ 3.30GHz
- 120 GiB of RAM (DDR3, 1333MHz)
- 16TB SAS HDDs (7,28TiB with RAID6)
- a RAID controller
- a controller for remote administration

So, what does it do? Not that much:

- It is one of my **backup** locations. ([borg](https://www.borgbackup.org/))
- It syncronizes my **calendar and contacts**. ([sabre/dav](https://sabre.io/dav/))
- It syncronizes my **files**. ([Syncthing](https://syncthing.net/))
- It hosts my **Matrix server**. ([Conduit](https://gitlab.com/famedly/conduit))
- It buffers my Notability backups. ([sabre/dav](https://sabre.io/dav/))

Okay, that list looks quite long. But I was just trying to trick you into thinking I was lying. It really is not that much :)

Apart from calendar and contact syncronization, all other services are new to my self-hosting configuration. Replacing external storage attached via USB with a RAID6 setup seems to be boosting confidence quite a bit for some reason …

Power draw is noticably higher, but that was to be expected. Memory is slower, storage is faster. Despite that, really happy with the server.

If you feel the need to tell me that I am totally wrong and should use my Raspberry Pi again, feel free to be [ignored](/#contact) by me ;)

That's it! My inflated ego has now deflated to normal levels again. By the way, did you know that I got a really cool new server?
