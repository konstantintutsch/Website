---
layout: page
permalink: /
title: Konstantin Tutsch
description: Welcome to Konstantin Tutsch's website. Discover lots of information about me, contact options, my photography and my personal blog.
---

## Recent posts

{% set counter = 0 %}
{% for post in collections.post | reverse %}
{% if counter < site.recent %}
{% post post %}
{% set counter = counter + 1 %}
{% endif %}
{% endfor %}

Blog - {% include "nav-blog.njk" %}

## Contact

Thought of feedback? Got questions? Or just want to chat? Tell me!

### Email

I can be reached by email. You can write me one at

{% include "email.md" %}

PGP - [`E0A9 6A34 8BBF 2168 D7A9 738C F0FA 0332 7B0E 8716`]({{ "pgp.txt" | inputPathToUrl }})

### Matrix

You can direct message me via Matrix. Contact me at {% social "matrix" %}

### Mastodon (Fediverse)

I'm also on Mastodon. You can follow me there at {% social "fediverse" %}

## Code

For all those interested in programming, you can take a look at my {% social "github", "GitHub" %} profile or the projects I have been working on:

- [Lock]({{ "Lock.md" | inputPathToUrl }}) - Process data with GnuPG 🔒
- [Caffeine]({{ "Caffeine.md" | inputPathToUrl }}) - Calculate your coffee ☕
