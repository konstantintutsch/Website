// NodeJS
var path = require("path");

// Plugins
const Image = require("@11ty/eleventy-img");

module.exports = {
    leadingZero: (number) => {
        number = number.toString();
        while (number.length < 2) number = "0" + number;
        while (number < 0 && number.length < 3) number = "-0" + number * -1;
        return number;
    },

    favicon: async (value) => {
        let directory = path.dirname(value);
        let widths = ["48", "96", "144", "180", "192"];

        let metadata = await new Image("./src" + value, {
            widths: widths,
            formats: ["png"],
            outputDir: "./build" + directory,
            filenameFormat: function (id, src, width, format, options) {
                const extension = path.extname(src);
                const base = path.basename(src, extension);

                return `${base}-${width}.${format}`;
            },
            urlPath: directory,
            useCache: true,
            svgAllowUpscale: true,
        });

        return widths;
    },
};
